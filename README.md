# Geocoder

Uses the [census.gov](https://geocoding.geo.census.gov/) public API to get GPS coordinates for US addresses. Batches are limited to 10,000 records.

Develiped by QBE Insurance Group Limited.

## Installation

```bash
pip install git+https://gitlab.com/qbeinnovation/geocoder
```

## Usage

This library has several methods with which to use to encode batches of addresses. Results are returned in a dictonary, with each location tied back to the given keys.

**geocode_dict** - Encode a specificly formatted dictonary of addresses:

```python
{'first':
    [
        {'office':
            [1,'1 GENERAL DRIVE','SUN PRAIRIE', 'WI', '53532'
        ]}
    ]
}
```

**batch_geocode** - encode a CSV formatted file. Each row should be in this order: `'key','address','city','state abv','zip'`.

## License

Licensed under the MIT open source license. See LICENSE.txt file.
