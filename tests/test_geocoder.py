"""
Tests for geocoder.
"""


class TestGeoCoder:

    def test_geocode_dict(self, geocoder):
        # Test for match
        location = {'office': ['1 GENERAL DRIVE', 'SUN PRAIRIE', 'WI', '53532']}
        result = geocoder.geocode_dict(location)
        assert "'lat': '0'" not in result

        # Test for no match
        location = {'office': ['1234 SOME ST', 'CITY', 'ST', '12345']}
        result = geocoder.geocode_dict(location)
        assert "'lat': '0'" in str(result)

    def test_batch_geocode(self, geocoder):
        data = open("tests/samples.csv", "r")
        result = geocoder.batch_geocode(data)
        assert len(result) == 4
