import pytest
from geocoder.geocoder import Geocoder


@pytest.fixture()
def geocoder():
    return Geocoder()
