from setuptools import setup

setup(
    name='Geocoder',
    version='0.1dev',
    packages=['geocoder', ],
    package_dir={'geocoder': 'geocoder'},
    license='MIT license',
    long_description=open('README.md').read(),
    install_requires=[
        'requests',
    ],
)
