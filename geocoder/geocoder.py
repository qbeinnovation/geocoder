import requests
import io

geocode_api = "https://geocoding.geo.census.gov/geocoder/locations/addressbatch"


class Geocoder(object):

    def geocode_dict(self, dict_to_geocode):
        '''
        Batch encode from a Policy dictionary
        '''
        csv_file = io.StringIO(self.format_dict(dict_to_geocode))
        return self.batch_geocode(csv_file)

    def format_dict(self, dict_to_format):
        '''
        Extract the relevant bits from the dictionary and convert to a CSV format
        '''
        csvString = ""
        for key, value in dict_to_format.items():
            csvString += key + ',' + \
                value[0] + ',' + value[1] + ',' + \
                value[2] + ',' + value[3] + '\n'
        return csvString

    def batch_geocode(self, file):
        '''
        Converts an Address to a Point
        '''
        lat_lon_dict = {}
        files = {
            'addressFile': ('batch_temp.csv', file, 'text/csv')
        }
        payload = {
            "benchmark": "9"
        }
        resp = requests.post(geocode_api,
                             files=files,
                             data=payload)
        if ("errors" in resp):
            return lat_lon_dict
        else:
            lines = resp.text.split("\n")
            for line in lines:
                if line == '':
                    continue
                cols = line.replace('"', '').split(',')

                if 'Match' in cols:
                    lat_lon = {"lat": cols[12], "lon": cols[11]}
                    lat_lon_dict[cols[0]] = lat_lon
                elif 'No_Match' in cols:
                    lat_lon_dict[cols[0]] = {"lat": '0', "lon": '0'}
                elif 'Tie' in cols:
                    lat_lon_dict[cols[0]] = {"lat": '0', "lon": '0'}
                else:
                    if len(cols) > 0:
                        lat_lon_dict[cols[0]] = {"lat": '0', "lon": '0'}
                        print('geocoding failure: Results do not meet expectations')
                    else:
                        print('geocoding failure: No key')
        return lat_lon_dict
